﻿using System;
using System.Threading;
using JsonRPC.Client;
using JsonRPC.Lib;

namespace JsonRPC.Console.Client
{
  internal class Program
  {
    private static void pak2c()
    {
      System.Console.WriteLine();
      System.Console.WriteLine("Press ANY key to continue...");
      System.Console.ReadKey(true);
    }

    private static void Main(string[] args)
    {
      const int CONCURRENT_USERS = 1000;
      string[] serverAddresses =
        {
          "net.pipe://localhost/Pipe-MembershipService"
        };

      try
      {
        var rand = new Random();
        for (var i = 0; i < CONCURRENT_USERS; i++)
        {
          var serverAddress = serverAddresses[rand.Next(0, 1)];
          var clientTester = new ClientTester(i, serverAddress);
          clientTester.Start();
        }

        var dateTimeService = JsonRpcClient.Factory.Create<IDateTimeServices>("net.pipe://localhost/DateTimeServices/");
        var result = dateTimeService.Now;
        System.Console.WriteLine("IDateTimeService.Now => {0}", result.ToLongDateString());
      }
      catch (Exception ex)
      {
        System.Console.WriteLine(ex);
      }

      pak2c();
    }

    private class ClientTester
    {
      private readonly int _clientId;
      private readonly string _serverAddress;

      public ClientTester(int clientId, string serverAddress)
      {
        _clientId = clientId;
        _serverAddress = serverAddress;
      }

      private void DoWork()
      {
        System.Console.WriteLine("Staring client [{0}]...", _clientId);

        // create a proxy of the service we would like to communicate with
        var membershipService = JsonRpcClient.Factory.Create<IMembershipService>(_serverAddress);

        try
        {
          var isAuthorized = membershipService.ValidateUser("userName", "pass@word");
          System.Console.WriteLine("[ CLIENT {0} ] - IMembershipService.Authorize => {1}", _clientId, isAuthorized);

          var user = membershipService.GetUser("userName");
          System.Console.WriteLine("[ CLIENT {0} ] - IMembershipService.GetUser => {1}", _clientId, user.Email);

          membershipService.SetPassword(user, 10, DateTime.Now.ToUniversalTime(), "pass@word123");
          System.Console.WriteLine("[ CLIENT {0} ] - IMembershipService.SetPassword => {1}", _clientId, user.Email);
        }
        catch (Exception ex)
        {
          System.Console.WriteLine("[ CLIENT {0} ] - Error: {1}", _clientId, ex);
        }

        System.Console.WriteLine("Shutting down client [{0}]...", _clientId);
      }

      public void Start()
      {
        new Thread(DoWork).Start();
      }
    }
  }
}
