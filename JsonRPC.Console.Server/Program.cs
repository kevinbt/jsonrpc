﻿using System;
using System.Diagnostics;
using System.Threading;

using JsonRPC.Lib;
using JsonRPC.Server;

namespace JsonRPC.Console.Server
{
  class Program
  {
    public static string FormatByte(long bytes, int rounding)
    {
      if (bytes >= Math.Pow(2, 80))
        return Math.Round(bytes/Math.Pow(2, 70), rounding).ToString() + " YB"; //yettabyte
      if (bytes >= Math.Pow(2, 70))
        return Math.Round(bytes/Math.Pow(2, 70), rounding).ToString() + " ZB"; //zettabyte
      if (bytes >= Math.Pow(2, 60))
        return Math.Round(bytes/Math.Pow(2, 60), rounding).ToString() + " EB"; //exabyte
      if (bytes >= Math.Pow(2, 50))
        return Math.Round(bytes/Math.Pow(2, 50), rounding).ToString() + " PB"; //petabyte
      if (bytes >= Math.Pow(2, 40))
        return Math.Round(bytes/Math.Pow(2, 40), rounding).ToString() + " TB"; //terabyte
      if (bytes >= Math.Pow(2, 30))
        return Math.Round(bytes/Math.Pow(2, 30), rounding).ToString() + " GB"; //gigabyte
      if (bytes >= Math.Pow(2, 20))
        return Math.Round(bytes/Math.Pow(2, 20), rounding).ToString() + " MB"; //megabyte
      if (bytes >= Math.Pow(2, 10))
        return Math.Round(bytes/Math.Pow(2, 10), rounding).ToString() + " KB"; //kilobyte

      return bytes.ToString() + " Bytes"; //byte
    }

    private static void pak2c()
    {
      System.Console.WriteLine();
      System.Console.WriteLine("Press ANY key to continue...");
      System.Console.ReadKey(true);
    }

    static void Main(string[] args)
    {
      System.Console.WriteLine("Staring server...");


      var currentProc = Process.GetCurrentProcess();
      var memoryUsedBefore = currentProc.PrivateMemorySize64;

      // simulate the creation of some "expensive" service :)
      IMembershipService membershipService = new MembershipService();
      IDateTimeServices dateTimeServices = new DateTimeServices();

      // the server may expose several different (single or multiple services) enpoints for communication
      string[] serviceUris =
          {
            "net.pipe://localhost/Pipe-MembershipService"
          };
      // "http://localhost:4503/MembershipService/",
      // use factories to create our main classes
      var engine = JsonRpcServer.ProcessingEngine.Factory.Create();
      var server = JsonRpcServer.Server.Factory.Create(engine);

      // register the service(s) to be exposed along with the associated endpoints
      server.Register(membershipService, serviceUris);
      server.Register(dateTimeServices, "net.pipe://localhost/DateTimeServices/");

      // begin listening
      using (server.Listen())
      {
        // this exists purely for the purposes to stopping the console application.
        // in a windows service, a stopper thread is created (in the background) to achieve the same result :)
        var stopThread = new Thread(() =>
        {
          // allow main thread to start...
          Thread.Sleep(TimeSpan.FromSeconds(1));

          System.Console.WriteLine("The JSON-RPC service is running....");
          System.Console.WriteLine("Press ANY key to stop the service.");
          System.Console.ReadKey(true);

          engine.Stop();

          System.Console.WriteLine("The JSON-RPC service is shutting down....");
        });
        stopThread.Start();

        // processes all incoming client requests on calling thread. 
        // this method will block until a key is pressed and invokes the Stop() method on the engine.
        engine.Start();
      }

      System.Console.WriteLine("Shutting down server...");

      currentProc = Process.GetCurrentProcess();
      var memoryUsedAfter = currentProc.PrivateMemorySize64;

      System.Console.WriteLine("Memory at startup: {0}", FormatByte(memoryUsedBefore, 3));
      System.Console.WriteLine("Memory at shutdown: {0}", FormatByte(memoryUsedAfter, 3));
      

      pak2c();
    }
  }
}
