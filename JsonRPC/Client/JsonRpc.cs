﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.DynamicProxy;
using JsonRPC.Channels;
using JsonRPC.Channels.Http;
using JsonRPC.Channels.NamedPipes;
using JsonRPC.Serialization.Newtonsoft;
using JsonRPC.Server;

namespace JsonRPC.Client
{
  /// <summary>
  /// A factory that creates clients to send messages to variously configured services.
  /// </summary>
  public interface IJsonRpcClientFactory
  {
    /// <summary>
    /// Creates a client of a specified type to the specified address.
    /// </summary>
    /// <typeparam name="T">The type of client that the factory creates.</typeparam>
    /// <param name="addressUri">The location of the service.</param>
    /// <returns></returns>
    T Create<T>(Uri addressUri);
  }
  
  /// <summary>
  /// Provides default client factory implementation.
  /// </summary>
  public static class JsonRpcClient
  {
    private static readonly IJsonRpcClientFactory _factory = new DefaultJsonRpcClientFactory();

    public static IJsonRpcClientFactory Factory
    {
      get { return _factory; }
    }
  }

  class DefaultJsonRpcClientFactory : IJsonRpcClientFactory
  {
    static DefaultJsonRpcClientFactory()
    {
      // TODO: this should be moved, pulled from config, etc.
      JsonRpcChannel.Register("http://", new HttpJsonRpcClientChannelCreator());
      JsonRpcChannel.Register("https://", new HttpJsonRpcClientChannelCreator());
      JsonRpcChannel.Register("net.pipe://", new NamedPipeJsonRpcClientChannelCreator());
    }

    public T Create<T>(Uri addressUri)
    {
      var clientChannel = JsonRpcChannel.Factory.CreateClientChannel(addressUri);
      var protocol = new DefaultJsonRpcProtocol();
      var generator = new ProxyGenerator();
      var interceptor = new JsonRpcMethodInterceptor(protocol, clientChannel);
      var proxy = generator.CreateInterfaceProxyWithoutTarget(typeof(T), interceptor);
      return (T)proxy;
    }
  }
}
