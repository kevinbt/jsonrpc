using System;
using Castle.DynamicProxy;
using JsonRPC.Channels;
using JsonRPC.Serialization;

namespace JsonRPC.Client
{
  internal class JsonRpcMethodInterceptor : IInterceptor
  {
    private readonly IJsonRpcProtocol _rpcProtocol;
    private readonly IJsonRpcClientChannel _channel;

    public JsonRpcMethodInterceptor(
        IJsonRpcProtocol rpcProtocol, 
        IJsonRpcClientChannel channel)
    {
      _rpcProtocol = rpcProtocol;
      _channel = channel;
    }

    public void Intercept(IInvocation invocation)
    {
      var serviceName = invocation.Method.DeclaringType.FullName.Replace('.', '_');
      var requestInfo = new JsonRpcRequest
                          {
                            Id = Guid.NewGuid().ToString("D"),
                            Params = invocation.Arguments,
                            Method = string.Format("{0}.{1}", serviceName, invocation.Method.Name)
                          };

      var requestData = _rpcProtocol.CreateRequest(requestInfo);
      string responseData;

      try
      {
        responseData = _channel.Invoke(requestData);
      }
      catch (Exception e)
      {
        throw new JsonRpcClientException("unable to get data from transport", e);
      }

      var response = _rpcProtocol.ReadResponse(responseData, invocation.Method.ReturnType);

      if (!string.Equals(requestInfo.Id, response.Id, StringComparison.InvariantCultureIgnoreCase))
      {
        throw new JsonRpcException("Request Id does not match response Id.");
      }

      if (response.HasError)
      {
        throw new JsonRpcRemoteException(response.Error.Code, response.Error.Message, response.Error.Data);
      }

      if (typeof (void) != invocation.Method.ReturnType)
      {
        invocation.ReturnValue = response.Result;
      }
    }
  }
}