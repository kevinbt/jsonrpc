﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC.Client
{
  public static class JsonRpcClientExtensions
  {
    /// <summary>
    /// Creates a client of a specified type to the specified address.
    /// </summary>
    /// <typeparam name="T">The type of client that the factory creates.</typeparam>
    /// <param name="clientFactory"></param>
    /// <param name="address">The location of the service.</param>
    /// <returns></returns>
    public static T Create<T>(this IJsonRpcClientFactory clientFactory, string address)
    {
      return clientFactory.Create<T>(new Uri(address));
    }
  }
}
