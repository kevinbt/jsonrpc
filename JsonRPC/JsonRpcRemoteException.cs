﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC
{
  /// <summary>
  /// 
  /// </summary>
  public class JsonRpcRemoteException : JsonRpcException
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public JsonRpcRemoteException(string message)
      : base(message)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorCode"></param>
    /// <param name="message"></param>
    /// <param name="data"></param>
    public JsonRpcRemoteException(int errorCode, string message, string data)
      : base(message)
    {
      ErrorCode = errorCode;
      ErrorData = data;
    }

    public int ErrorCode { get; private set; }
    public string ErrorData { get; private set; }
  }
}
