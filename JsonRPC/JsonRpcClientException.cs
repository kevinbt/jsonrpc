﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC
{
  /// <summary>
  /// 
  /// </summary>
  public class JsonRpcClientException : JsonRpcException
  {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public JsonRpcClientException(string message)
      : base(message)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public JsonRpcClientException(String message, Exception innerException)
      : base(message, innerException)
    {

    }
  }
}
