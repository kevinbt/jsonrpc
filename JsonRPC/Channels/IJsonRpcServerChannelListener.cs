using System;

namespace JsonRPC.Channels
{
  /// <summary>
  /// Represents a protocol listener.
  /// </summary>
  public interface IJsonRpcServerChannelListener
  {
    /// <summary>
    /// Begins asynchronously retrieving an incoming request.
    /// </summary>
    /// <param name="callback">An <see cref="AsyncCallback"/> delegate that references the method to invoke when a request is available.</param>
    /// <returns>An <see cref="IAsyncResult"/> object that that indicates the status of the asynchronous operation.</returns>
    IAsyncResult BeginAccept(AsyncCallback callback);
    
    /// <summary>
    /// Completes an asynchronous operation to retrieve an incoming client request.
    /// </summary>
    /// <param name="result">An <see cref="IAsyncResult"/> object that was obtained when the asynchronous operation was started.</param>
    /// <returns>An <see cref="IJsonRpcConnectedClient"/> object that represents the connected client.</returns>
    IJsonRpcConnectedClient EndAccept(IAsyncResult result);
    
    /// <summary>
    /// Waits (synchronously) for an incoming request and returns when one is received.
    /// </summary>
    /// <returns>An <see cref="IJsonRpcConnectedClient"/> object that represents the connected client.</returns>
    IJsonRpcConnectedClient Accept();
  }
}