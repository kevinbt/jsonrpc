﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using JsonRPC.Server;

namespace JsonRPC.Channels
{
  /// <summary>
  /// Defines the contract of a communication channel.
  /// </summary>
  public interface IJsonRpcChannel
  {
    /// <summary>
    /// Performs any initialization required for the channel.
    /// </summary>
    void Open();

    /// <summary>
    /// Closes the current channel and releases any associated resources.
    /// </summary>
    void Close();
  }

  /// <summary>
  /// Provides the interface for creating <see cref="IJsonRpcClientChannel"/> instances.
  /// </summary>
  public interface IJsonRpcClientChannelCreator
  {
    /// <summary>
    /// Creates a <see cref="IJsonRpcClientChannel"/> instance. 
    /// </summary>
    /// <param name="uri">The uniform resource identifier (URI) of the target service.</param>
    /// <returns>A <see cref="IJsonRpcClientChannel"/> instance.</returns>
    IJsonRpcClientChannel Create(Uri uri);
  }

  /// <summary>
  /// Provides the interface for creating <see cref="IJsonRpcServerChannel"/> instances.
  /// </summary>
  public interface IJsonRpcServerChannelCreator
  {
    /// <summary>
    /// Creates a <see cref="IJsonRpcServerChannel"/> instance.
    /// </summary>
    /// <param name="uri">The uniform resource identifier (URI) of the service.</param>
    /// <returns>A <see cref="IJsonRpcServerChannel"/> instance.</returns>
    IJsonRpcServerChannel Create(Uri uri);
  }
}
