using System;

namespace JsonRPC.Channels
{
  /// <summary>
  /// Represents a client which is communicating with the server.
  /// </summary>
  public interface IJsonRpcConnectedClient : IDisposable
  {
    /// <summary>
    /// Reads the client incoming JSON-RPC request.
    /// </summary>
    /// <returns>A string containing the client request.</returns>
    string ReadRequest();

    /// <summary>
    /// Write the specific JSON-RPC response back to the client.
    /// </summary>
    /// <param name="responseData">The response to be sent to the client.</param>
    void WriteResponse(string responseData);
  }
}