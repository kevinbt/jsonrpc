﻿namespace JsonRPC.Channels
{
  /// <summary>
  /// Defines the method of a service which establishes data connection with a known server.
  /// </summary>
  public interface IJsonRpcClientChannel : IJsonRpcChannel
  {
    /// <summary>
    /// Invokes a remote method request.
    /// </summary>
    /// <param name="requestData">The JSON-RPC request to be submitted.</param>
    /// <returns>a string containing the JSON-RPC server response.</returns>
    string Invoke(string requestData);
  }
}
