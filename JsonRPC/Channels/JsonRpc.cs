﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.DynamicProxy;
using JsonRPC.Channels;
using JsonRPC.Serialization.Newtonsoft;
using JsonRPC.Server;

namespace JsonRPC.Channels
{
  /// <summary>
  /// 
  /// </summary>
  public interface IJsonRpcChannelFactory
  {
    /// <summary>
    /// Initializes a new <see cref="IJsonRpcClientChannel"/> instance for the specified URI scheme.
    /// </summary>
    /// <param name="addressUri">A <see cref="System.Uri"/> containing the URI of the requested resource.</param>
    /// <returns></returns>
    IJsonRpcClientChannel CreateClientChannel(Uri addressUri);

    /// <summary>
    /// Initializes a new <see cref="IJsonRpcServerChannel"/> instance for the specified URI scheme.
    /// </summary>
    /// <param name="addressUri">A <see cref="System.Uri"/> containing the URI of the requested resource.</param>
    /// <returns></returns>
    IJsonRpcServerChannel CreateServerChannel(Uri addressUri);

    /// <summary>
    /// Registers a <see cref="IJsonRpcClientChannelCreator"/> descendant for the specified URI.
    /// </summary>
    /// <param name="protocol">The complete URI or URI protocol that the <b>IJsonRpcClientChannel</b> descendant services.</param>
    /// <param name="channelCreator">The instance to create the <see cref="IJsonRpcClientChannel"/> descendant.</param>
    /// <returns>true if registration is successful; otherwise, false.</returns>
    bool RegisterClientChannel(string protocol, IJsonRpcClientChannelCreator channelCreator);

    /// <summary>
    /// Registers a <see cref="IJsonRpcServerChannelCreator"/> descendant for the specified URI.
    /// </summary>
    /// <param name="protocol">The complete URI or URI protocol that the <b>IJsonRpcServerChannel</b> descendant services.</param>
    /// <param name="channelCreator">The instance to create the <see cref="IJsonRpcServerChannel"/> descendant.</param>
    /// <returns>true if registration is successful; otherwise, false.</returns>
    bool RegisterServerChannel(string protocol, IJsonRpcServerChannelCreator channelCreator);
  }

  /// <summary>
  /// 
  /// </summary>
  public static class JsonRpcChannel
  {
    private static readonly IJsonRpcChannelFactory _factory = new DefaultJsonRpcChannelFactory();

    /// <summary>
    /// 
    /// </summary>
    public static IJsonRpcChannelFactory Factory
    {
      get { return _factory; }
    }

    /// <summary>
    /// Registers a <see cref="IJsonRpcClientChannelCreator"/> descendant for the specified URI.
    /// </summary>
    /// <param name="protocol">The complete URI or URI protocol that the <b>IJsonRpcClientChannel</b> descendant services.</param>
    /// <param name="channelCreator">The instance to create the <see cref="IJsonRpcClientChannel"/> descendant.</param>
    /// <returns>true if registration is successful; otherwise, false.</returns>
    public static bool Register(string protocol, IJsonRpcClientChannelCreator channelCreator)
    {
      return Factory.RegisterClientChannel(protocol, channelCreator);
    }

    /// <summary>
    /// Registers a <see cref="IJsonRpcServerChannelCreator"/> descendant for the specified URI.
    /// </summary>
    /// <param name="protocol">The complete URI or URI protocol that the <b>IJsonRpcServerChannel</b> descendant services.</param>
    /// <param name="channelCreator">The instance to create the <see cref="IJsonRpcServerChannel"/> descendant.</param>
    /// <returns>true if registration is successful; otherwise, false.</returns>
    public static bool Register(string protocol, IJsonRpcServerChannelCreator channelCreator)
    {
      return Factory.RegisterServerChannel(protocol, channelCreator);
    }
  }
}
