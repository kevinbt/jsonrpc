﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using JsonRPC.Server;

namespace JsonRPC.Channels.Http
{
  public sealed class HttpJsonRpcClientChannel : IJsonRpcClientChannel
  {
    private readonly Uri _endPointUrl;
    private readonly IDictionary<string, string> _headers; 

    public HttpJsonRpcClientChannel(Uri endpointUrl)
    {
      _endPointUrl = endpointUrl;
      _headers = new Dictionary<string, string>();
    }

    private WebRequest MakePostRequest(string requestData)
    {
      var request = WebRequest.Create(_endPointUrl) as HttpWebRequest;
      request.Method = "POST";
      var byteArray = Encoding.UTF8.GetBytes(requestData);
      request.ContentType = "application/x-www-form-urlencoded";
      request.ContentLength = byteArray.Length;

      foreach (var header in _headers)
      {
        request.Headers.Add(header.Key, header.Value);
      }

      var dataStream = request.GetRequestStream();
      dataStream.Write(byteArray, 0, byteArray.Length);
      dataStream.Close();

      return request;
    }

    private static string ReadResponse(WebRequest request)
    {
      string responseData;
      using (var response = request.GetResponse())
      {
        var responseStatusCode = ((HttpWebResponse) response).StatusCode;
        if (HttpStatusCode.OK != responseStatusCode)
          throw new JsonRpcClientException(string.Format("Unexpected Response Status Code: {0}", responseStatusCode));

        var dataStream = response.GetResponseStream();
        using (var reader = new StreamReader(dataStream))
        {
          responseData = reader.ReadToEnd();
        }
      }

      return responseData;
    }

    public void AddHeader(string key, string value)
    {
      _headers.Add(key, value);
    }

    public string Invoke(string requestData)
    {
      var webRequest = MakePostRequest(requestData);
      var responseData = ReadResponse(webRequest);
      return responseData;
    }

    public void Open()
    {
      
    }

    public void Close()
    {
      
    }
  }
}
