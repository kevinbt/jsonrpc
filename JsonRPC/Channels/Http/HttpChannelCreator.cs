﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC.Channels.Http
{
  public sealed class HttpJsonRpcClientChannelCreator : IJsonRpcClientChannelCreator
  {
    public IJsonRpcClientChannel Create(Uri uri)
    {
      return new HttpJsonRpcClientChannel(uri);
    }
  }

  public sealed class HttpJsonRpcServerChannelCreator : IJsonRpcServerChannelCreator
  {
    private HttpJsonRpcServerChannel _singleServerChannel;

    public IJsonRpcServerChannel Create(Uri uri)
    {
      if (null == _singleServerChannel)
      {
        _singleServerChannel = new HttpJsonRpcServerChannel(uri);
      }
      else
      {
        _singleServerChannel.RegisterEndPoint(uri);
      }

      return _singleServerChannel;
    }
  }
}
