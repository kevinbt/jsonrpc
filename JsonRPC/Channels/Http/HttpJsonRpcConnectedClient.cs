﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace JsonRPC.Channels.Http
{
  internal class HttpJsonRpcConnectedClient : IJsonRpcConnectedClient
  {
    private readonly HttpListenerContext _context;

    public HttpJsonRpcConnectedClient(HttpListenerContext context)
    {
      _context = context;
    }

    public string ReadRequest()
    {
      string requestContents;
      using (var inputStream = new StreamReader(_context.Request.InputStream))
      {
        requestContents = inputStream.ReadToEnd();
      }

      return requestContents;
    }

    public void WriteResponse(string responseData)
    {
      _context.Response.ContentType = "application/json";
      _context.Response.ContentEncoding = Encoding.UTF8;

      var buf = Encoding.UTF8.GetBytes(responseData);
      _context.Response.ContentLength64 = buf.Length;
      _context.Response.OutputStream.Write(buf, 0, buf.Length);
    }

    public void Dispose()
    {
      
    }
  }
}
