﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using JsonRPC.Server;

namespace JsonRPC.Channels.Http
{
  public sealed class HttpJsonRpcServerChannel : IJsonRpcServerChannel
  {
    private readonly HttpListener _httpListener;

    public HttpJsonRpcServerChannel(Uri uri)
    {
      _httpListener = new HttpListener();
      RegisterEndPoint(uri);
    }

    public void Open()
    {
      if (_httpListener.IsListening)
        throw new InvalidOperationException("The server channel is already opened.");

      _httpListener.Start();
    }

    public IJsonRpcServerChannelListener Listen()
    {
      return new HttpJsonRpcServerChannelListener(_httpListener);
    }

    public void Close()
    {
      if (!_httpListener.IsListening)
        throw new InvalidOperationException("The server channel is already closed.");
      
      _httpListener.Close();
    }

    public void RegisterEndPoint(Uri uri)
    {
      if (_httpListener.IsListening)
        throw new InvalidOperationException("The server channel is already opened.");

      var url = uri.ToString();
      if (!url.EndsWith("/"))
      {
        url = string.Format("{0}/", url);
      }

      _httpListener.Prefixes.Add(url);
    }
  }
}
