using System;
using System.Net;

namespace JsonRPC.Channels.Http
{
  internal class HttpJsonRpcServerChannelListener : IJsonRpcServerChannelListener
  {
    private readonly HttpListener _httpListener;

    internal HttpJsonRpcServerChannelListener(HttpListener listener)
    {
      _httpListener = listener;
    }

    public IAsyncResult BeginAccept(AsyncCallback callback)
    {
      return _httpListener.BeginGetContext(callback, null);
    }

    public IJsonRpcConnectedClient EndAccept(IAsyncResult result)
    {
      var context = _httpListener.EndGetContext(result);
      return new HttpJsonRpcConnectedClient(context);
    }

    public IJsonRpcConnectedClient Accept()
    {
      var context = _httpListener.GetContext();
      return new HttpJsonRpcConnectedClient(context);
    }
  }
}