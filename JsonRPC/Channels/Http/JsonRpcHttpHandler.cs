﻿using System.Web;
using JsonRPC.Serialization.Newtonsoft;
using JsonRPC.Server;

namespace JsonRPC.Channels.Http
{
  public sealed class JsonRpcHttpHandler : IHttpHandler
  {
    public bool IsReusable
    {
      get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
      var jsonRpcProtocol = new DefaultJsonRpcProtocol();
      var jsonRpcServer = new JsonRpcProcessor(jsonRpcProtocol);
      var serverChannel = new HttpJsonRpcServerChannel(context);

      jsonRpcServer.Process(serverChannel);
    }
  }
}
