﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace JsonRPC.Channels
{
  class DefaultJsonRpcChannelFactory : IJsonRpcChannelFactory
  {

    private static readonly object sync = new object();
    private static List<ChannelEntry> _channelEntries; 

    static DefaultJsonRpcChannelFactory()
    {
      Init();
    }

    public IJsonRpcClientChannel CreateClientChannel(Uri addressUri)
    {
      return CreateChannel<ClientChannelEntry, IJsonRpcClientChannel>(
        addressUri, false, (entry) => entry.Creator.Create(addressUri)
      );
    }

    public IJsonRpcServerChannel CreateServerChannel(Uri addressUri)
    {
      return CreateChannel<ServerChannelEntry, IJsonRpcServerChannel>(
        addressUri, false, (entry) => entry.Creator.Create(addressUri)
      );
    }

    public bool RegisterClientChannel(string protocol, IJsonRpcClientChannelCreator channelCreator)
    {
      return RegisterChannelCreator(new ClientChannelEntry(protocol, channelCreator));
    }

    public bool RegisterServerChannel(string protocol, IJsonRpcServerChannelCreator channelCreator)
    {
      return RegisterChannelCreator(new ServerChannelEntry(protocol, channelCreator));
    }

    private static TChannel CreateChannel<TChannelEntry, TChannel>(Uri serviceAddressUri, bool useUriBase, Func<TChannelEntry, TChannel> creatorAction)
      where TChannelEntry : ChannelEntry
      where TChannel : class, IJsonRpcChannel
    {
      var absoluteUri = (!useUriBase) ? serviceAddressUri.AbsoluteUri : serviceAddressUri.Scheme + ':';
      var length = absoluteUri.Length;

      foreach (var channelEntry in ChannelCreators.OfType<TChannelEntry>())
      {
        if ((length < channelEntry.Protocol.Length) ||
            (string.Compare(channelEntry.Protocol, 0, absoluteUri, 0, channelEntry.Protocol.Length,
                            StringComparison.OrdinalIgnoreCase) != 0)) continue;
        return creatorAction(channelEntry);
      }

      throw new JsonException(string.Format("The service protocol '{0}' is not supported.", serviceAddressUri.Scheme));
    }

    private static bool RegisterChannelCreator<T>(T channelEntry) where T : ChannelEntry
    {
      ChannelEntry existingEntry;
      lock (sync)
      {
        existingEntry =
          ChannelCreators.OfType<T>()
            .FirstOrDefault(
              entry =>
              ((channelEntry.Protocol.Length == entry.Protocol.Length) &&
               (0 == string.Compare(entry.Protocol, channelEntry.Protocol, StringComparison.OrdinalIgnoreCase)))
            );

        if (null == existingEntry)
        {
          ChannelCreators.Add(channelEntry);
        }
      }


      return (null == existingEntry);
    }

    private static void Init()
    {
      if (null != _channelEntries)
        return;

      lock (sync)
      {
        if (null != _channelEntries)
          return;

        _channelEntries = new List<ChannelEntry>();
      }
    }

    private static List<ChannelEntry> ChannelCreators
    {
      get
      {
        // Longer prefixes should be evaluated before
        _channelEntries.Sort((x, y) => string.Compare(y.Protocol, x.Protocol, StringComparison.OrdinalIgnoreCase));
        return _channelEntries;
      }
    }
  }
}
