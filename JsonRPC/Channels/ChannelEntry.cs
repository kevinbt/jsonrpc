﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace JsonRPC.Channels
{
  internal abstract class ChannelEntry
  {

    protected ChannelEntry(string protocol, Type creatorType)
    {
      Protocol = protocol;
      CreatorType = creatorType;
    }

    public string Protocol { get; private set; }

    public Type CreatorType { get; private set; }
  }

  internal class ServerChannelEntry : ChannelEntry
  {
    private IJsonRpcServerChannelCreator _creator;

    public ServerChannelEntry(string protocol, Type creatorType)
      : base(protocol, creatorType)
    {
      if (!typeof (IJsonRpcServerChannelCreator).IsAssignableFrom(creatorType))
        throw new ArgumentException("Expected", "creatorType");
    }

    public ServerChannelEntry(string protocol, IJsonRpcServerChannelCreator creator)
      : this(protocol, creator.GetType())
    {
      _creator = creator;
    }

    public IJsonRpcServerChannelCreator Creator
    {
      get
      {
        if (null != _creator)
          return _creator;

        lock (this)
        {
          _creator = (IJsonRpcServerChannelCreator) Activator.CreateInstance(
            CreatorType, BindingFlags.CreateInstance | BindingFlags.NonPublic |
                         BindingFlags.Public | BindingFlags.Instance, null, new object[0],
            CultureInfo.InvariantCulture);
        }

        return _creator;
      }
    }
  }

  internal class ClientChannelEntry : ChannelEntry
  {
    private IJsonRpcClientChannelCreator _creator;

    public ClientChannelEntry(string protocol, Type creatorType)
      : base(protocol, creatorType)
    {
      if (!typeof(IJsonRpcClientChannelCreator).IsAssignableFrom(creatorType))
        throw new ArgumentException("Expected", "creatorType");
    }

    public ClientChannelEntry(string protocol, IJsonRpcClientChannelCreator creator)
      : this(protocol, creator.GetType())
    {
      _creator = creator;
    }

    public IJsonRpcClientChannelCreator Creator
    {
      get
      {
        if (null != _creator)
          return _creator;

        lock (this)
        {
          _creator = (IJsonRpcClientChannelCreator) Activator.CreateInstance(
            CreatorType, BindingFlags.CreateInstance | BindingFlags.NonPublic |
                         BindingFlags.Public | BindingFlags.Instance, null, new object[0],
            CultureInfo.InvariantCulture);
        }

        return _creator;
      }
    }
  }
}
