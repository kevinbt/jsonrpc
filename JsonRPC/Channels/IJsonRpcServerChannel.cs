﻿using System;

namespace JsonRPC.Channels
{
  /// <summary>
  /// Defines the method of a service which responds to connections from a client.
  /// </summary>
  public interface IJsonRpcServerChannel : IJsonRpcChannel
  {
    /// <summary>
    /// Puts the channel to a listen state.
    /// </summary>
    /// <returns>A <see cref="IJsonRpcServerChannelListener"/> object used to manage incoming requests.</returns>
    IJsonRpcServerChannelListener Listen();
  }
}
