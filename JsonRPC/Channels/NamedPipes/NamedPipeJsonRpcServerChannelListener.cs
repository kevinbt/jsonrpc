﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace JsonRPC.Channels.NamedPipes
{
  internal class NamedPipeJsonRpcServerChannelListener : IJsonRpcServerChannelListener
  {
    private readonly Uri _namedPipeServerUri;

    internal NamedPipeJsonRpcServerChannelListener(Uri uri)
    {
      _namedPipeServerUri = uri;
    }

    private NamedPipeServerStream CreateNamedPipe()
    {
      return NamedPipeServerStreamFactory.Create(_namedPipeServerUri);
    }

    public IAsyncResult BeginAccept(AsyncCallback callback)
    {
      var pipeStream = CreateNamedPipe();
      return pipeStream.BeginWaitForConnection(callback, pipeStream);
    }

    public IJsonRpcConnectedClient EndAccept(IAsyncResult result)
    {
      var pipeStream = (NamedPipeServerStream) result.AsyncState;
      pipeStream.EndWaitForConnection(result);
      return new NamedPipeJsonRpcConnectedClient(pipeStream);
    }

    public IJsonRpcConnectedClient Accept()
    {
      var pipeStream = CreateNamedPipe();
      pipeStream.WaitForConnection();

      return new NamedPipeJsonRpcConnectedClient(pipeStream);
    }
  }
}
