﻿using System;
using System.IO;
using System.IO.Pipes;
using JsonRPC.Server;

namespace JsonRPC.Channels.NamedPipes
{
  public sealed class NamedPipeJsonRpcServerChannel : IJsonRpcServerChannel
  {
    private readonly Uri _pipeServerUri;

    public NamedPipeJsonRpcServerChannel(Uri uri)
    {
      _pipeServerUri = uri;
    }

    public void Open()
    {

    }

    public void Close()
    {

    }

    public IJsonRpcServerChannelListener Listen()
    {
      return new NamedPipeJsonRpcServerChannelListener(_pipeServerUri);
    }
  }
}
