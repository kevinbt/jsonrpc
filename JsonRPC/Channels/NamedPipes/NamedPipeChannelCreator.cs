﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;

namespace JsonRPC.Channels.NamedPipes
{
  public sealed class NamedPipeJsonRpcClientChannelCreator : IJsonRpcClientChannelCreator
  {
    public IJsonRpcClientChannel Create(Uri uri)
    {
      return new NamedPipeJsonRpcClientChannel(uri);
    }
  }

  internal class NamedPipeClientStreamFactory
  {
    public static NamedPipeClientStream Create(Uri uri)
    {
      var serverName = uri.Host;
      var pipeName = uri.LocalPath.Substring(1);

      return new NamedPipeClientStream(
        serverName, pipeName,
        PipeDirection.InOut,
        PipeOptions.WriteThrough
      );
    }
  }

  public sealed class NamedPipeJsonRpcServerChannelCreator : IJsonRpcServerChannelCreator
  {
    public IJsonRpcServerChannel Create(Uri uri)
    {
      return new NamedPipeJsonRpcServerChannel(uri);
    }
  }

  internal class NamedPipeServerStreamFactory
  {
    private const int PIPE_UNLIMITED_INSTANCES = 255;
    private const int IN_BUFFER_SIZE = 4096;
    private const int OUT_BUFFER_SIZE = 4096;

    public static NamedPipeServerStream Create(Uri uri)
    {
      var pipeName = string.Format("\\.{0}", uri.LocalPath).Replace('/', '\\');
      var pipeSecurity = new PipeSecurity();
      pipeSecurity.AddAccessRule(new PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow));
      pipeSecurity.AddAccessRule(new PipeAccessRule(WindowsIdentity.GetCurrent().Name, PipeAccessRights.FullControl,
                                                    AccessControlType.Allow));
      pipeSecurity.AddAccessRule(new PipeAccessRule("SYSTEM", PipeAccessRights.FullControl, AccessControlType.Allow));


      return new NamedPipeServerStream(
        pipeName, PipeDirection.InOut,
        NamedPipeServerStream.MaxAllowedServerInstances, 
        PipeTransmissionMode.Message,
        PipeOptions.WriteThrough | PipeOptions.Asynchronous, 
        IN_BUFFER_SIZE, OUT_BUFFER_SIZE, pipeSecurity
        );
    }
  }
}
