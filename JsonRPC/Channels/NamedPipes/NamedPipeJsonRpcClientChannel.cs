﻿using System;
using System.IO;
using System.IO.Pipes;
using JsonRPC.Server;

namespace JsonRPC.Channels.NamedPipes
{
  public sealed class NamedPipeJsonRpcClientChannel : IJsonRpcClientChannel
  {
    private const int BLOCK_SIZE = 4096;
    private readonly NamedPipeClientStream _namedPipeClient;

    public NamedPipeJsonRpcClientChannel(Uri uri)
    {
      _namedPipeClient = NamedPipeClientStreamFactory.Create(uri);
    }


    private void WriteRequest(string requestData)
    {
      using (var stream = new MemoryStream())
      {
        var writer = new StreamWriter(stream);
        writer.Write(requestData);
        writer.Flush();

        stream.Seek(0, SeekOrigin.Begin);
        var buf = stream.ToArray();

        _namedPipeClient.Write(buf, 0, buf.Length);
      }
    }

    private string ReadResponse()
    {
      using (var stream = new MemoryStream())
      {
        var buf = new byte[BLOCK_SIZE];
        int count;
        while ((count = _namedPipeClient.Read(buf, 0, BLOCK_SIZE)) > 0)
        {
          stream.Write(buf, 0, count);
        }

        stream.Seek(0, SeekOrigin.Begin);
        using (var reader = new StreamReader(stream))
        {
          return reader.ReadToEnd();
        }
      }
    }

    public string Invoke(string requestData)
    {
      _namedPipeClient.Connect();
      WriteRequest(requestData);
      return ReadResponse();
    }

    public void Open()
    {
      
    }

    public void Close()
    {
      
    }
  }
}
