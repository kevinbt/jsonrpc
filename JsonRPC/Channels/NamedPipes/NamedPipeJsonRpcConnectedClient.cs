﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace JsonRPC.Channels.NamedPipes
{
  internal class NamedPipeJsonRpcConnectedClient : IJsonRpcConnectedClient
  {
    private const int BLOCK_SIZE = 4096;
    private readonly NamedPipeServerStream _namedPipeServer;

    internal  NamedPipeJsonRpcConnectedClient(NamedPipeServerStream namedPipeServer)
    {
      _namedPipeServer = namedPipeServer;
    }

    public string ReadRequest()
    {
      using (var stream = new MemoryStream())
      {
        var buf = new byte[BLOCK_SIZE];
        do
        {
          var count = _namedPipeServer.Read(buf, 0, BLOCK_SIZE);
          stream.Write(buf, 0, count);

        } while (!_namedPipeServer.IsMessageComplete);

        stream.Seek(0, SeekOrigin.Begin);
        using (var reader = new StreamReader(stream))
        {
          return reader.ReadToEnd();
        }
      }
    }

    public void WriteResponse(string responseData)
    {
      using (var stream = new MemoryStream())
      {
        var writer = new StreamWriter(stream);
        writer.Write(responseData);
        writer.Flush();

        stream.Seek(0, SeekOrigin.Begin);
        var buf = stream.ToArray();

        _namedPipeServer.Write(buf, 0, buf.Length);
      }
    }

    public void Dispose()
    {
      _namedPipeServer.Dispose();
    }
  }
}
