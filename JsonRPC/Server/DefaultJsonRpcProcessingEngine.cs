﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JsonRPC.Server
{
  class DefaultJsonRpcProcessingEngine : TaskScheduler, IJsonRpcProcessingEngine
  {
    private bool _isRunning;
    private ManualResetEventSlim _eventSlim;
    private ConcurrentQueue<Task> _taskQueue;

    public DefaultJsonRpcProcessingEngine()
    {
      _taskQueue = new ConcurrentQueue<Task>();
    }

    public void Start()
    {
      if (_isRunning)
        throw new InvalidOperationException("Processing engine is already running.");
      
      _isRunning = true;
      DoWork();
    }

    public void Stop()
    {
      Submit(() =>
               {
                 _isRunning = false;
               });
    }

    public void Submit(Action action)
    {
      var task = new Task(action);
      task.Start(this);
    }

    protected override IEnumerable<Task> GetScheduledTasks()
    {
      yield break;
    }

    protected override void QueueTask(Task task)
    {
      _taskQueue.Enqueue(task);
      if (null != _eventSlim)
      {
        _eventSlim.Set();
      }
    }

    protected override bool TryDequeue(Task task)
    {
      Task outTask;
      _taskQueue.TryDequeue(out outTask);
      return (outTask == task);
    }

    protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
    {
      if (taskWasPreviouslyQueued && !TryDequeue(task))
      {
        return false;
      }

      return TryExecuteTask(task);
    }

    private void DoWork()
    {
      _eventSlim = new ManualResetEventSlim();
      while (true)
      {
        Task task = null;
        if (_taskQueue.TryDequeue(out task))
        {

          TryExecuteTask(task);

          if (!_isRunning)
          {
            _taskQueue = new ConcurrentQueue<Task>();
            break;
          }
        }
        else
        {
          _eventSlim.Wait();
          _eventSlim.Reset();
        }
      }

      _eventSlim.Dispose();
      _eventSlim = null;
    }
  }
}
