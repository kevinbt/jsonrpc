﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using JsonRPC.Channels;
using JsonRPC.Serialization;
using JsonRPC.Serialization.Newtonsoft;

namespace JsonRPC.Server
{
  internal sealed class JsonRpcProcessor
  {
    private const string METHOD_PATTERN = "([_a-zA-Z][_a-zA-Z0-9]*)\\.([_a-zA-Z][_a-zA-Z0-9]*)";

    private readonly IJsonRpcProtocol _jsonRpcProtocol;

    public JsonRpcProcessor(IJsonRpcProtocol protocol)
    {
      _jsonRpcProtocol = protocol;
    }

    private void SendError(IJsonRpcConnectedClient client, JsonRpcRequest request, JsonRpcError error)
    {
      var response = new JsonRpcResponse
                       {
                         Error = error,
                         Id = (null == request) ? null : request.Id
                       };

      var errorResponse = _jsonRpcProtocol.CreateResponse(response);
      client.WriteResponse(errorResponse);
    }

    private JsonRpcRequest ReadRequest(IJsonRpcConnectedClient client, out string requestData)
    {
      JsonRpcRequest request;
      requestData = null;
      try
      {
        requestData = client.ReadRequest();
        request = _jsonRpcProtocol.ReadRequest(requestData);
      }
      catch (Exception ex)
      {
        request = null;
        var error = new JsonRpcError
                      {
                        Code = JsonRpcErrorCodes.PARSE_ERROR,
                        Message = "An error occurred on the server while parsing the JSON text.",
                        Data = ex.StackTrace
                      };
        SendError(client, null, error);
      }

      return request;
    }

    private bool VerifyRequest(IJsonRpcConnectedClient client, JsonRpcRequest request)
    {
      if (string.IsNullOrEmpty(request.Id) || string.IsNullOrEmpty(request.Method))
      {
        var error = new JsonRpcError
                      {
                        Code = JsonRpcErrorCodes.INVALID_REQUEST,
                        Message = "The JSON sent is not a valid Request object."
                      };
        SendError(client, request, error);
        return false;
      }

      return true;
    }

    private JsonRpcResponse ExecuteMethod(IJsonRpcConnectedClient client, JsonRpcRequest request, IDictionary<string, JsonRpcService> services)
    {
      var response = new JsonRpcResponse
                       {
                         Id = request.Id
                       };

      try
      {
        var match = Regex.Match(request.Method, METHOD_PATTERN);
        if (!match.Success)
        {
          var message = string.Format("The method name '{0}' is not valid.", request.Method);
          throw new JsonRpcRemoteException(JsonRpcErrorCodes.INVALID_REQUEST, "Invalid Method Name", message);
        }

        var serviceName = match.Groups[1].Value;
        var methodName = match.Groups[2].Value;

        JsonRpcService serviceEntry;
        if (!services.TryGetValue(serviceName, out serviceEntry))
        {
          var message = string.Format("The service name '{0}' does not exist.", serviceName);
          throw new JsonRpcRemoteException(JsonRpcErrorCodes.METHOD_NOT_FOUND, "No such service/method exists.", message);
        }

        var serviceMethod = serviceEntry.GetMethod(methodName, request.Params);
        if (null == serviceMethod)
        {
          var message = string.Format("The method name '{0}' does not exist.", methodName);
          throw new JsonRpcRemoteException(JsonRpcErrorCodes.METHOD_NOT_FOUND, "No such service/method exists.", message);
        }

        var parameterTypes = serviceMethod.GetParameters().Select(x => x.ParameterType).ToArray();
        _jsonRpcProtocol.ReadParameters(request, parameterTypes);
        response.Result = serviceMethod.Invoke(serviceEntry.Instance, request.Params);

      }
      catch (JsonRpcRemoteException jrre)
      {
        response = null;
        var error = new JsonRpcError
                      {
                        Code = jrre.ErrorCode,
                        Message = jrre.Message,
                        Data = jrre.ErrorData
                      };
        SendError(client, request, error);
      }
      catch (Exception ex)
      {
        response = null;
        var error = new JsonRpcError
                      {
                        Code = JsonRpcErrorCodes.INTERNAL_ERROR,
                        Message = ex.Message,
                        Data = ex.StackTrace
                      };
        SendError(client, request, error);
      }

      return response;
    }

    public void Process(IJsonRpcConnectedClient client, IDictionary<string, JsonRpcService> services)
    {
      string requestData;
      var request = ReadRequest(client, out requestData);
      if (null == request)
        return;

      if (!VerifyRequest(client, request))
        return;

      var response = ExecuteMethod(client, request, services);
      if (null == response)
        return;

      var responseData = _jsonRpcProtocol.CreateResponse(response);
      client.WriteResponse(responseData);
    }
  }
}
