﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using JsonRPC.Channels;

namespace JsonRPC.Server
{
  class DefaultJsonRpcServer : IJsonRpcServer
  {
    private readonly JsonRpcProcessor _processor;
    private readonly IJsonRpcProcessingEngine _processingEngine;
    private readonly IDictionary<IJsonRpcServerChannel, object> _channels; 
    private readonly IDictionary<string, JsonRpcService> _services;

    public DefaultJsonRpcServer(IJsonRpcProtocol protocol, IJsonRpcProcessingEngine processingEngine)
    {
      _processingEngine = processingEngine;
      _processor = new JsonRpcProcessor(protocol);
      _channels = new Dictionary<IJsonRpcServerChannel, object>();
      _services = new Dictionary<string, JsonRpcService>();
    }

    public void Register<T>(T serviceInstance, params Uri[] addressUris)
    {
      if (null == addressUris || addressUris.Length == 0)
      {

      }

      lock (_services)
      {
        var key = typeof (T).FullName.Replace('.', '_');
        if (_services.ContainsKey(key))
        {
          throw new ArgumentException("The service has already been registered.");
        }
        CreateChannels(addressUris);
        _services.Add(key, new JsonRpcService(serviceInstance, typeof (T)));
      }
    }

    private void CreateChannels(IEnumerable<Uri> addressUris)
    {
      foreach (var addressUri in addressUris)
      {
        var channel = JsonRpcChannel.Factory.CreateServerChannel(addressUri);
        if (!_channels.ContainsKey(channel))
        {
          _channels.Add(channel, null);
        }
      }
    }

    public IDisposable Listen()
    {
      foreach (var channel in _channels.Keys)
      {
        channel.Open();
        var listener = channel.Listen();
        AcceptNextClient(listener);
      }

      return new DisposableObject(Close);
    }

    private void AcceptNextClient(IJsonRpcServerChannelListener listener)
    {
      try
      {
        listener.BeginAccept(
          iasr =>
            {
              IJsonRpcConnectedClient client = null;
              try
              {
                client = listener.EndAccept(iasr);
                AcceptNextClient(listener);
              }
              catch (ObjectDisposedException)
              {
                return;
              }

              _processingEngine.Submit(() =>
                                         {
                                           _processor.Process(client, _services);
                                           client.Dispose();
                                         });

            });
      }
      catch (Exception e)
      {
        throw;
      }
    }

    private void Close()
    {
      foreach (var channel in _channels.Keys)
      {
        channel.Close();
      }
    }
  }
}
