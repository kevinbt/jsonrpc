﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using JsonRPC.Channels;

namespace JsonRPC.Server
{
  internal class JsonRpcService
  {
    internal JsonRpcService(object service, Type interfaceType)
    {
      Instance = service;
      ServiceType = interfaceType;
    }

    public object Instance { get; private set; }
    public Type ServiceType { get; private set; }

    public MethodInfo GetMethod(string methodName, object[] arguments)
    {
      if (null == arguments) arguments = new object[0];
      return ServiceType.GetMethods(BindingFlags.Public | BindingFlags.Instance)
        .Where(m => string.Equals(m.Name, methodName, StringComparison.InvariantCultureIgnoreCase))
        .SingleOrDefault(m => m.GetParameters().Length == arguments.Length);
    }
  }
}
