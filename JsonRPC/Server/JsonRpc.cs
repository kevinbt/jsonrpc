﻿using System;
using JsonRPC.Channels;
using JsonRPC.Channels.Http;
using JsonRPC.Channels.NamedPipes;
using JsonRPC.Serialization.Newtonsoft;

namespace JsonRPC.Server
{
  /// <summary>
  /// Defines a factory for creating <see cref="IJsonRpcServer"/> instances.
  /// </summary>
  public interface IJsonRpcServerFactory
  {
    /// <summary>
    /// Creates a <see cref="IJsonRpcServer"/> instance.
    /// </summary>
    /// <param name="processingEngine">The <see cref="IJsonRpcProcessingEngine"/> instance to be used.</param>
    /// <returns>A new <see cref="IJsonRpcServer"/> instance.</returns>
    IJsonRpcServer Create(IJsonRpcProcessingEngine processingEngine);

    /// <summary>
    /// Creates a <see cref="IJsonRpcServer"/> instance.
    /// </summary>
    /// <param name="protocol"></param>
    /// <param name="processingEngine">The <see cref="IJsonRpcProcessingEngine"/> instance to be used.</param>
    /// <returns>A new <see cref="IJsonRpcServer"/> instance.</returns>
    IJsonRpcServer Create(IJsonRpcProtocol protocol, IJsonRpcProcessingEngine processingEngine);
  }

  /// <summary>
  /// Defines a factory for creating <see cref="IJsonRpcProcessingEngine"/> instances.
  /// </summary>
  public interface IJsonRpcIProcessingEngineFactory
  {
    /// <summary>
    /// Creates a <see cref="IJsonRpcProcessingEngine"/> instance.
    /// </summary>
    /// <returns>A new <see cref="IJsonRpcProcessingEngine"/> instance.</returns>
    IJsonRpcProcessingEngine Create();
  }

  /// <summary>
  /// Defines the services for a JSON-RPC server.
  /// </summary>
  public interface IJsonRpcServer
  {
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="serviceInstance"></param>
    /// <param name="addresses"></param>
    void Register<T>(T serviceInstance, params Uri[] addresses);

    /// <summary>
    /// Begins listening for incoming client requests.
    /// </summary>
    /// <returns></returns>
    IDisposable Listen();
  }

  /// <summary>
  /// Defines the contract of service which manages tasks to be executed.
  /// </summary>
  public interface IJsonRpcProcessingEngine
  {
    /// <summary>
    /// Starts the processing engine.
    /// </summary>
    void Start();

    /// <summary>
    /// Stops the processing engine.
    /// </summary>
    void Stop();

    /// <summary>
    /// Submits a task to be executed by the processing engine.
    /// </summary>
    /// <param name="action">The task to be executed.</param>
    void Submit(Action action);
  }

  /// <summary>
  /// Provides default server factory implementation.
  /// </summary>
  public static class JsonRpcServer
  {
    /// <summary>
    /// Default <see cref="IJsonRpcServerFactory"/> fatory.
    /// </summary>
    public class Server
    {
      private static readonly IJsonRpcServerFactory _factory = new DefaultJsonRpcServerFactory();

      /// <summary>
      /// Gets the <see cref="IJsonRpcServerFactory"/> instance.
      /// </summary>
      public static IJsonRpcServerFactory Factory
      {
        get { return _factory; }
      }
    }

    /// <summary>
    /// Default <see cref="IJsonRpcIProcessingEngineFactory"/> fatory.
    /// </summary>
    public class ProcessingEngine
    {
      private static readonly IJsonRpcIProcessingEngineFactory _factory = new DefaultJsonRpcProcessingEngineFactory();

      /// <summary>
      /// Gets the <see cref="IJsonRpcIProcessingEngineFactory"/> instance.
      /// </summary>
      public static IJsonRpcIProcessingEngineFactory Factory
      {
        get { return _factory; }
      }
    }
  }

  class DefaultJsonRpcServerFactory : IJsonRpcServerFactory
  {
    static DefaultJsonRpcServerFactory()
    {
      // TODO: this should be moved, pulled from config, etc.
      JsonRpcChannel.Register("http://", new HttpJsonRpcServerChannelCreator());
      JsonRpcChannel.Register("https://", new HttpJsonRpcServerChannelCreator());
      JsonRpcChannel.Register("net.pipe://", new NamedPipeJsonRpcServerChannelCreator());
    }

    public IJsonRpcServer Create(IJsonRpcProcessingEngine processingEngine)
    {
      return Create(new DefaultJsonRpcProtocol(), processingEngine);
    }

    public IJsonRpcServer Create(IJsonRpcProtocol protocol, IJsonRpcProcessingEngine processingEngine)
    {
      return new DefaultJsonRpcServer(protocol, processingEngine);
    }
  }
  
  class DefaultJsonRpcProcessingEngineFactory : IJsonRpcIProcessingEngineFactory
  {
    public IJsonRpcProcessingEngine Create()
    {
#if NET40
      return new DefaultJsonRpcProcessingEngine();
#elif NET35
      return new JsonRpcProcessingEngineSlim();
#endif
    }
  }
}
