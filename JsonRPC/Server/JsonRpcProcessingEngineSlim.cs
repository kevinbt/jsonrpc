﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using JsonRPC.Utilities;

namespace JsonRPC.Server
{
  class JsonRpcProcessingEngineSlim : IJsonRpcProcessingEngine
  {
    private bool _isRunning;
    private ManualResetEvent _manualResetEvent;
    private NonBlockingQueue<Task> _taskQueue;

    public JsonRpcProcessingEngineSlim()
    {
      _taskQueue = new NonBlockingQueue<Task>();
    }

    public void Start()
    {
      if (_isRunning)
        throw new InvalidOperationException("Processing engine is already running.");

      _isRunning = true;
      DoWork();
    }

    public void Stop()
    {
      Submit(() =>
      {
        _isRunning = false;
        _manualResetEvent.Set();
      });
    }

    public void Submit(Action action)
    {
      var task = new Task(action);
      task.Start(this);
    }

    private void QueueTask(Task task)
    {
      _taskQueue.Enqueue(task);
      if (null != _manualResetEvent)
      {
        _manualResetEvent.Set();
      }
    }

    private void DoWork()
    {
      _manualResetEvent = new ManualResetEvent(false);
      while (true)
      {
        Task task = null;
        if (_taskQueue.TryDequeue(out task))
        {
          task.FireAndForget();
          if (!_isRunning)
            break;
        }
        else
        {
          if (!_isRunning)
            break;
          
          _manualResetEvent.WaitOne();
          _manualResetEvent.Reset();
        }
      }

      _taskQueue = new NonBlockingQueue<Task>();
      _manualResetEvent.Close();
      _manualResetEvent = null;
    }

    private class Task
    {
      private readonly Action _action;

      public Task(Action action)
      {
        _action = action;
      }

      public void FireAndForget()
      {
        _action.BeginInvoke(iar =>
                              {
                                var action = (Action) iar.AsyncState;
                                action.EndInvoke(iar);
                              }, _action);
      }

      public void Start(JsonRpcProcessingEngineSlim engine)
      {
        engine.QueueTask(this);
      }
    }
  }
}
