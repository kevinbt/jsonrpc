﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC.Server
{
  public static class JsonRpcServerExtensions
  {
    public static void Register<T>(this IJsonRpcServer server, T serviceInstance, params string[] addresses)
    {
      if (null == addresses || addresses.Length == 0)
      {
        throw new ArgumentNullException();
      }

      var addressUris = addresses.Select(x => new Uri(x)).ToArray();
      server.Register(serviceInstance, addressUris);
    }
  }
}
