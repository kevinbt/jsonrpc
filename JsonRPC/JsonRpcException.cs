﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC
{
  /// <summary>
  /// 
  /// </summary>
  public class JsonRpcException : ApplicationException
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public JsonRpcException(string message)
      : base(message)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public JsonRpcException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
