﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JsonRPC.Serialization;

namespace JsonRPC
{
  /// <summary>
  /// Defines the method of a service which creating and parsing JSON-RPC strings.
  /// </summary>
  public interface IJsonRpcProtocol
  {
    /// <summary>
    /// Gets the JSON-RPC protocol version.
    /// </summary>
    string Version { get; }

    /// <summary>
    /// Creates a JSON-RPC request.
    /// </summary>
    /// <param name="requestInfo">The request information to be used.</param>
    /// <returns>a string containing the request.</returns>
    string CreateRequest(JsonRpcRequest requestInfo);

    /// <summary>
    /// Creates a JSON-RPC response.
    /// </summary>
    /// <param name="responseInfo">The response information to be used.</param>
    /// <returns>a string containing the response.</returns>
    string CreateResponse(JsonRpcResponse responseInfo);

    /// <summary>
    /// Parses a JSON-RPC request.
    /// </summary>
    /// <param name="requestData">The JSON-RPC request string.</param>
    /// <returns>A <see cref="JsonRpcRequest"/> object containing the request details.</returns>
    JsonRpcRequest ReadRequest(string requestData);

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="requestData"></param>
    ///// <param name="request"></param>
    ///// <param name="parameterTypes"></param>
    //void ReadParameters(string requestData, JsonRpcRequest request, Type[] parameterTypes);
    void ReadParameters(JsonRpcRequest request, Type[] types);

    /// <summary>
    /// Parses a JSON-RPC response.
    /// </summary>
    /// <param name="responseData"></param>
    /// <returns>A <see cref="JsonRpcResponse"/> object containing the response details.</returns>
    JsonRpcResponse ReadResponse(string responseData);

    /// <summary>
    /// Parses a JSON-RPC response.
    /// </summary>
    /// <param name="responseData"></param>
    /// <param name="returnType"></param>
    /// <returns>A <see cref="JsonRpcResponse"/> object containing the response details.</returns>
    JsonRpcResponse ReadResponse(string responseData, Type returnType);
  }
}
