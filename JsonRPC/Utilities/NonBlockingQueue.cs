﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace JsonRPC.Utilities
{
  public sealed class NonBlockingQueue<T>
  {
    private SinglyLinkedNode<T> _head;
    private SinglyLinkedNode<T> _tail;

    public NonBlockingQueue()
    {
      _head = _tail = new SinglyLinkedNode<T>(default(T));
    }

    public NonBlockingQueue(IEnumerable<T> collection)
      : this()
    {
      EnqueueRange(collection);
    }

    public void Enqueue(T item)
    {
      var newNode = new SinglyLinkedNode<T>(item);
      for (;;)
      {
        var curTail = _tail;
        if (Interlocked.CompareExchange(ref curTail.Next, newNode, null) == null)
          //append to the tail if it is indeed the tail.
        {
          Interlocked.CompareExchange(ref _tail, newNode, curTail);
          //CAS in case we were assisted by an obstructed thread.
          return;
        }
        Interlocked.CompareExchange(ref _tail, curTail.Next, curTail); //assist obstructing thread.
      }
    }

    public void EnqueueRange(IEnumerable<T> collection)
    {
      SinglyLinkedNode<T> start;
      SinglyLinkedNode<T> end;
      using (var en = collection.GetEnumerator())
      {
        if (!en.MoveNext())
          return;
        start = end = new SinglyLinkedNode<T>(en.Current);
        while (en.MoveNext())
          end = end.Next = new SinglyLinkedNode<T>(en.Current);
      }
      for (;;)
      {
        var curTail = _tail;
        if (Interlocked.CompareExchange(ref curTail.Next, start, null) == null)
        {
          for (;;)
          {
            var newTail = Interlocked.CompareExchange(ref _tail, end, curTail);
            if (newTail == curTail || newTail.Next == null)
              return;
            end = newTail;
            for (var next = end.Next; next != null; next = (end = next).Next) ;
          }
        }
        Interlocked.CompareExchange(ref _tail, curTail.Next, curTail);
      }
    }

    public bool TryDequeue(out T item)
    {
      for (;;)
      {
        var curHead = _head;
        var curTail = _tail;
        var curHeadNext = curHead.Next;
        if (curHead == curTail)
        {
          if (curHeadNext == null)
          {
            item = default(T);
            return false;
          }
          Interlocked.CompareExchange(ref _tail, curHeadNext, curTail); // assist obstructing thread
        }
        if (Interlocked.CompareExchange(ref _head, curHeadNext, curHead) != curHead) continue;
        item = curHeadNext.Item;
        return true;
      }
    }

    public bool TryPeek(out T item)
    {
      var node = _head.Next;
      if (node == null)
      {
        item = default(T);
        return false;
      }
      item = node.Item;
      return true;
    }

    public bool IsEmpty
    {
      get
      {
        var head = _head;
        return head == _tail && head.Next == null;
      }
    }
  }
}
