﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonRPC.Utilities
{

  internal sealed class SinglyLinkedNode<T>
  {
    public T Item;
    public SinglyLinkedNode<T> Next;
    public SinglyLinkedNode(T item)
    {
      Item = item;
    }
  }
}
