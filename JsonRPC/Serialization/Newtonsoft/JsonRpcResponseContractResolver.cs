﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace JsonRPC.Serialization.Newtonsoft
{
  internal sealed class JsonRpcResponseContractResolver : CamelCasePropertyNamesContractResolver
  {
    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
      var property = base.CreateProperty(member, memberSerialization);
      switch (property.PropertyName)
      {
        case "hasError":
          property.ShouldSerialize = (instance) => false;
          break;
        case "error":
          property.ShouldSerialize =
            (instance) =>
              {
                var resp = (JsonRpcResponse) instance;
                return (null != resp.Error);
              };
          break;
        case "result":
          property.ShouldSerialize =
            (instance) =>
              {
                var resp = (JsonRpcResponse) instance;
                return (null == resp.Error);
              };
          break;
      }

      return property;
    }
  }
}
