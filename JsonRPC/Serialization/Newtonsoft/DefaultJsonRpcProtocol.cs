﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonRPC.Serialization.Newtonsoft
{
  public sealed class DefaultJsonRpcProtocol : IJsonRpcProtocol
  {
    public string Version
    {
      get { return "2.0"; }
    }

    public string CreateRequest(JsonRpcRequest requestInfo)
    {
      requestInfo.Jsonrpc = Version;
      return JsonConvert.SerializeObject(requestInfo,
                                         Formatting.None,
                                         new JsonSerializerSettings
                                         {
                                           ContractResolver =
                                             new JsonRpcRequestContractResolver()
                                         });


    }

    public string CreateResponse(JsonRpcResponse responseInfo)
    {
      responseInfo.Jsonrpc = Version;
      return JsonConvert.SerializeObject(responseInfo,
                                         Formatting.None,
                                         new JsonSerializerSettings
                                           {
                                             ContractResolver =
                                               new JsonRpcResponseContractResolver()
                                           });
    }

    public JsonRpcRequest ReadRequest(string requestData)
    {
      return JsonConvert.DeserializeObject<JsonRpcRequest>(requestData);
    }

    public void ReadParameters(JsonRpcRequest request, Type[] types)
    {
      var arguments = new List<object>();
      for (var idx = 0; idx < request.Params.Length; idx++)
      {
        var rawValue = request.Params[idx];
        var rawValueType = rawValue.GetType();
        var paramType = types[idx];

        if (!paramType.IsAssignableFrom(rawValueType))
        {
          var typedObj = JsonConvert.DeserializeObject(rawValue.ToString(), types[idx]);
          arguments.Add(typedObj);  
        } else
        {
          arguments.Add(rawValue);
        }

      }
      request.Params = arguments.ToArray();
    }

    public JsonRpcResponse ReadResponse(string responseData)
    {
      return ReadResponse(responseData, typeof (void));
    }

    public JsonRpcResponse ReadResponse(string responseData, Type returnType)
    {
      var response = JsonConvert.DeserializeObject<JsonRpcResponse>(responseData);
      if (typeof(void) != returnType)
      {
        var rawResult = response.Result;
        var rawValueType = rawResult.GetType();
        if (!returnType.IsAssignableFrom(rawValueType))
        {
          response.Result = JsonConvert.DeserializeObject(rawResult.ToString(), returnType);
        }
      }

      return response;
    }
  }
}
