﻿using Newtonsoft.Json.Linq;

namespace JsonRPC.Serialization.Newtonsoft
{
  internal static class JsonSerializationHelper
  {
    public static bool IsPrimitive(this JToken token)
    {
      if (null == token)
        return false;

      switch (token.Type)
      {
        case JTokenType.Integer:
        case JTokenType.Float:
        case JTokenType.String:
        case JTokenType.Boolean:
          return true;
      }

      return false;
    }

    public static bool IsObject(this JToken token)
    {
      return (null != token) && (JTokenType.Object == token.Type);
    }
  }
}
