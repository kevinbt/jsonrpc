using System;

namespace JsonRPC.Serialization
{
  public class JsonRpcRequest
  {
    public string Jsonrpc { get; set; }
    public string Id { get; set; }
    public string Method { get; set; }
    public object[] Params { get; set; }
  }

  public class JsonRpcResponse
  {
    public string Jsonrpc { get; set; }
    public string Id { get; set; }
    public object Result { get; set; }
    public JsonRpcError Error { get; set; }

    public bool HasError
    {
      get { return null != Error; }
    }
  }

  public class JsonRpcError
  {
    public int Code { get; set; }
    public string Message { get; set; }
    public string Data { get; set; }
  }

  public static class JsonRpcErrorCodes
  {

    /// <summary>
    /// Invalid JSON was received by the server. An error occurred on the server while parsing the JSON text.
    /// </summary>
    public const int PARSE_ERROR = -32700;

    /// <summary>
    /// The JSON sent is not a valid Request object.
    /// </summary>
    public const int INVALID_REQUEST = -32600;

    /// <summary>
    /// The method does not exist / is not available.
    /// </summary>
    public const int METHOD_NOT_FOUND = -32601;

    /// <summary>
    /// Invalid method parameter(s).
    /// </summary>
    public const int INVALID_PARAMS = -32602;

    /// <summary>
    /// Internal JSON-RPC error.
    /// </summary>
    public const int INTERNAL_ERROR = -32603;

  }
}