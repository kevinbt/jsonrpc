﻿using System;

namespace JsonRPC.Lib
{
  /// <summary>
  /// Defines the contract for a service which provides DateTime services.
  /// </summary>
  public interface IDateTimeServices
  {
    /// <summary>
    /// Gets the current <see cref="DateTime"/> using the default application's time zone.
    /// </summary>
    DateTime Now { get; }
  }
}
