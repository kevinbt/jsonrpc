﻿using System;
using System.Web.Security;

namespace JsonRPC.Lib
{
  public class User
  {
    public virtual string UserName { get; set; }
    public virtual string Email { get; set; }
    public virtual string LoweredUserName { get; set; }

    public virtual string TimeZone { get; set; }

    public virtual string FirstName { get; set; }
    public virtual string LastName { get; set; }

    public virtual string Password { get; set; }
    public virtual int PasswordFormat { get; set; }

    public virtual MembershipPasswordFormat PasswordFormatType
    {
      get { return (MembershipPasswordFormat) PasswordFormat; }
      set { PasswordFormat = (int) value; }
    }

    public virtual string HashAlgorithm { get; set; }
    public virtual string PasswordSalt { get; set; }

    public virtual bool IsApproved { get; set; }
    public virtual bool IsLockedOut { get; set; }

    public virtual DateTime CreatedOn { get; set; }
    public virtual DateTime LastLoginDate { get; set; }
    public virtual DateTime LastLockoutDate { get; set; }

    public virtual int FailedPasswordAttemptCount { get; set; }
    public virtual DateTime FailedPasswordAttemptWindowStart { get; set; }
  }
}
