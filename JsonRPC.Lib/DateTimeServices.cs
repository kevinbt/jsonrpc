﻿using System;

namespace JsonRPC.Lib
{
  public class DateTimeServices : IDateTimeServices
  {
    public DateTime Now
    {
      get { return DateTime.UtcNow; }
    }
  }
}
