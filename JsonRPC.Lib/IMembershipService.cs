﻿using System;

namespace JsonRPC.Lib
{
  public interface IMembershipService
  {
    User GetUser(string userNameOrEmail);

    bool ValidateUser(string userNameOrEmail, string password);

    void SetPassword(User user, int number, DateTime date, string password);
  }
}
