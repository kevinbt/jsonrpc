﻿using System;

namespace JsonRPC.Lib
{
  public class MembershipService : IMembershipService
  {
    public bool ValidateUser(string userName, string password)
    {
      return (userName == "userName") && (password == "pass@word");
    }

    public User GetUser(string userNameOrEmail)
    {
      // testing return type object

      return new User
               {
                 CreatedOn = DateTime.Now,
                 Email = "userName@domain.com",
                 FailedPasswordAttemptCount = 0,
                 FailedPasswordAttemptWindowStart = DateTime.MinValue,
                 FirstName = "FirstName",
                 HashAlgorithm = "SHA-1",
                 IsApproved = true,
                 IsLockedOut = false,
                 LastLockoutDate = new DateTime(2012, 11, 12, 10, 0, 0),
                 LastLoginDate = new DateTime(2012, 11, 12, 10, 0, 0),
                 TimeZone = "UTC−04:00"
               };
    }

    public void SetPassword(User user, int number, DateTime date, string password)
    {
      // no nothing
      // testing parameter object graph & void return type
    }
  }
}
